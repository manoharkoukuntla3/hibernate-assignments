package entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="employee_assign_three")
public class EmployeeAssignThree {
	@Id
	@Column(name="id")
	private int id;
	@Column(name="name")
	private String name;

	
	@OneToOne(cascade=CascadeType.ALL)
	private Passport passport;
	
	
	public EmployeeAssignThree() {
		super();
	}
	public EmployeeAssignThree(int id, String name, Passport passport) {
		super();
		this.id = id;
		this.name = name;
		this.passport = passport;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public Passport getPassport() {
		return passport;
	}
	public void setPassport(Passport passport) {
		this.passport = passport;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EmployeeAssignThree : id=").append(id).append(", name=").append(name).append(", passport=").append(passport);
		return builder.toString();
	}

}
