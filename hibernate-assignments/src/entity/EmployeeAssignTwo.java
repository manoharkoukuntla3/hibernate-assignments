package entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="employee_assignment_two")
public class EmployeeAssignTwo {
	
	@Id
	@Column(name="id")
	private int id;
	
	@Column(name="name")
	private String name;
	@Column(name="salary")
	private float salary;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="dept_id")
	private DepartmentAssignTwo department;

	public EmployeeAssignTwo() {
		super();
	}

	public EmployeeAssignTwo(int id, String name, float salary, DepartmentAssignTwo department) {
		super();
		this.id = id;
		this.name = name;
		this.salary = salary;
		this.department = department;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getSalary() {
		return salary;
	}

	public void setSalary(float salary) {
		this.salary = salary;
	}

	public DepartmentAssignTwo getDepartment() {
		return department;
	}

	public void setDepartment(DepartmentAssignTwo department) {
		this.department = department;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EmployeeAssignTwo : id=").append(id).append(", name=").append(name).append(", salary=")
				.append(salary).append(", department=").append(department);
		return builder.toString();
	}
	
}
