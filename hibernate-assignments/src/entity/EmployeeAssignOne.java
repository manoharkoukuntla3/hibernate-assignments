package entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="employee_assignment_one")
public class EmployeeAssignOne
{
	
	public EmployeeAssignOne() {
		super();
	}
	public EmployeeAssignOne(int id, String name, String band) {
		super();
		this.id = id;
		this.name = name;
		this.band = band;
	}
	@Id
	@Column(name="id")
	private int id;
	@Column(name="name")
	private String name;
	@Column(name="band")
	private String band;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBand() {
		return band;
	}
	public void setBand(String band) {
		this.band = band;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EmployeeAssignOne : id=").append(id).append(", name=").append(name).append(", band=").append(band);
		return builder.toString();
	}
	
}
