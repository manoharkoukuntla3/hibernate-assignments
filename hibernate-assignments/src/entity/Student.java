package entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.JoinColumn;

@Entity
@Table(name="student")
public class Student {

	@Id
	@Column(name="student_id")
	private int id;
	
	@Column(name="name")
	private String name;
	
	@ManyToMany(cascade=CascadeType.ALL,fetch=FetchType.EAGER)
	@JoinTable(name="student_course", joinColumns=@JoinColumn(name="student_id",nullable=true),
	inverseJoinColumns=@JoinColumn(name="course_id",nullable=true))
	private Set<Course>registeredCourses;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Course> getRegisteredCourses() {
		return registeredCourses;
	}

	public void setRegisteredCourses(Set<Course> registeredCourses) {
		this.registeredCourses = registeredCourses;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Student : id=").append(id).append(", name=").append(name).append(", registeredCourses=")
				.append(registeredCourses);
		return builder.toString();
	}
	
}
