package entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="passport_details")
public class Passport {
	@Id
	@Column(name="passport_number")
	private long passportNumber;
	
	@Column(name="country")
	private String country;
	
	@Column(name="issue_date")
	 @Temporal(TemporalType.DATE)
	private Calendar issueDate;
	
	@Column(name="expiry_date")
	 @Temporal(TemporalType.DATE)
	private Calendar expiryDate;

	
	public Passport() {
		super();
	}


	public Passport(long passportNumber, String country, Calendar issueDate, Calendar expiryDate) {
		super();
		this.passportNumber = passportNumber;
		this.country = country;
		this.issueDate = issueDate;
		this.expiryDate = expiryDate;
	}


	public long getPassportNumber() {
		return passportNumber;
	}

	public void setPassportNumber(long passportNumber) {
		this.passportNumber = passportNumber;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}


	public Calendar getIssueDate() {
		return issueDate;
	}


	public void setIssueDate(Calendar issueDate) {
		this.issueDate = issueDate;
	}


	public Calendar getExpiryDate() {
		return expiryDate;
	}


	public void setExpiryDate(Calendar expiryDate) {
		this.expiryDate = expiryDate;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Passport : passportNumber=").append(passportNumber).append(", country=").append(country)
				.append(", issueDate=").append(issueDate).append(", expiryDate=").append(expiryDate);
		return builder.toString();
	}


	
	
}
