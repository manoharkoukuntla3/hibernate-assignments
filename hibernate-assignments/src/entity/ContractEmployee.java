package entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="contract_employee")
@PrimaryKeyJoinColumn(name="employee_id")
public class ContractEmployee extends EmployeeAssignFive {

	@Column(name="allowance")
	private float allowance;

	public float getAllowance() {
		return allowance;
	}

	public void setAllowance(float allowance) {
		this.allowance = allowance;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(super.toString()).append("ContractEmployee : allowance=").append(allowance);
		return builder.toString();
	}
	
}
