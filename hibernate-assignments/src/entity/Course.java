package entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="course")
public class Course {
	
	@Id
	@Column(name="course_id")
	private int id;
	
	@Column(name="course_name")
	private String name;
	
	@ManyToMany(cascade=CascadeType.ALL,mappedBy="registeredCourses",fetch=FetchType.EAGER)
	private Set<Student>registeredStudents;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Student> getRegisteredStudents() {
		return registeredStudents;
	}

	public void setRegisteredStudents(Set<Student> registeredStudents) {
		this.registeredStudents = registeredStudents;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Course : id=").append(id).append(", name=").append(name).append(", registeredStudents=")
				.append(registeredStudents);
		return builder.toString();
	}
	
	
}
