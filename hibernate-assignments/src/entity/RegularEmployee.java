package entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="regular_employee")
@PrimaryKeyJoinColumn(name="employee_id")
public class RegularEmployee extends EmployeeAssignFive {

	@Column(name="qplc")
	private float qplc;

	public float getQplc() {
		return qplc;
	}

	public void setQplc(float qplc) {
		this.qplc = qplc;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(super.toString()).append("RegularEmployee : qplc=").append(qplc);
		return builder.toString();
	}
	
}
