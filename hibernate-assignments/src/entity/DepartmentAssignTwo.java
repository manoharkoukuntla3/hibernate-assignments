package entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="department")
public class DepartmentAssignTwo {
	
	@Id
	@Column(name="dept_id")
	private int id;
	
	@Column(name="dept_name")
	private String name;
	
	@OneToMany(mappedBy="department")
	private Set<EmployeeAssignTwo>employeeSet;

	
	public DepartmentAssignTwo() {
		super();
	}


	public DepartmentAssignTwo(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Set<EmployeeAssignTwo> getEmployeeSet() {
		return employeeSet;
	}


	public void setEmployeeSet(Set<EmployeeAssignTwo> employeeSet) {
		this.employeeSet = employeeSet;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DepartmentAssignTwo : id=").append(id).append(", name=").append(name).append(", employeeSet=")
				.append(employeeSet);
		return builder.toString();
	}
	
	
}
