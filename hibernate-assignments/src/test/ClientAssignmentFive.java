package test;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import entity.ContractEmployee;
import entity.EmployeeAssignFive;
import entity.RegularEmployee;


public class ClientAssignmentFive {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SessionFactory factory=new Configuration()
				.configure()
				.addAnnotatedClass(EmployeeAssignFive.class)
				.addAnnotatedClass(RegularEmployee.class)
				.addAnnotatedClass(ContractEmployee.class)
				.buildSessionFactory();

		Session session=factory.openSession();
		session.beginTransaction();
		RegularEmployee re1=new RegularEmployee();
		re1.setEmployeeId(1);
		re1.setEmployeeName("Tony Stark");
		re1.setSalary(9098989800809F);
		re1.setQplc(3465686789F);
		RegularEmployee re2=new RegularEmployee();
		re2.setEmployeeId(2);
		re2.setEmployeeName("Hulk");
		re2.setSalary(987966998998F);
		re2.setQplc(346568679F);
		ContractEmployee ce1=new ContractEmployee();
		ce1.setEmployeeId(3);
		ce1.setEmployeeName("QuickSilver");
		ce1.setSalary(787877676767678F);
		ce1.setAllowance(5674645335F);
		ContractEmployee ce2=new ContractEmployee();
		ce2.setEmployeeId(4);
		ce2.setEmployeeName("Phil");
		ce2.setSalary(5760006767678F);
		ce2.setAllowance(234645335F);
		session.save(re1);
		session.save(re2);
		session.save(ce1);
		session.save(ce2);
		session.getTransaction().commit();
		System.out.println(session.get(EmployeeAssignFive.class, 1));
		System.out.println(session.get(EmployeeAssignFive.class, 2));
		System.out.println(session.get(EmployeeAssignFive.class, 3));
		System.out.println(session.get(EmployeeAssignFive.class, 4));
		
	}

}
