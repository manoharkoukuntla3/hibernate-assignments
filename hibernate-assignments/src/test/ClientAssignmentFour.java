package test;

import java.util.HashSet;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import entity.Course;
import entity.Student;


public class ClientAssignmentFour {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SessionFactory factory=new Configuration()
				.configure()
				.addAnnotatedClass(Student.class)
				.addAnnotatedClass(Course.class)
				.buildSessionFactory();

		Session session=factory.openSession();
		session.beginTransaction();
		Student s1=new Student();
		s1.setId(1);
		s1.setName("Thor");
		s1.setRegisteredCourses(new HashSet<Course>());
		Student s2=new Student();
		s2.setId(2);
		s2.setName("Hulk");
		s2.setRegisteredCourses(new HashSet<Course>());
		Course c1=new Course();
		c1.setId(1);
		c1.setName("Thunder Lessons");
		c1.setRegisteredStudents(new HashSet<Student>());
		Course c2=new Course();
		c2.setId(2);
		c2.setName("Smashing Lessons");
		c2.setRegisteredStudents(new HashSet<Student>());
		s1.getRegisteredCourses().add(c1);
		s2.getRegisteredCourses().add(c2);
	
		session.save(s1);
		session.save(s2);
		session.getTransaction().commit();
		System.out.println(session.get(Student.class,1));
		System.out.println(session.get(Student.class,2));
		System.out.println(session.get(Course.class,1));
		System.out.println(session.get(Course.class,2));
		}

}
