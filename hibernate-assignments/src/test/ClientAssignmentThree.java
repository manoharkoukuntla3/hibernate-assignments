package test;



import java.util.GregorianCalendar;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import entity.EmployeeAssignOne;
import entity.EmployeeAssignThree;
import entity.Passport;

public class ClientAssignmentThree {

	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SessionFactory factory=new Configuration()
				.configure()
				.addAnnotatedClass(EmployeeAssignThree.class)
				.addAnnotatedClass(Passport.class)
				.buildSessionFactory();

		Session session=factory.openSession();
		Passport pOne=new Passport(1234,"asgard",new GregorianCalendar(2018,0,1),new GregorianCalendar(2020,0,1));
		Passport pTwo=new Passport(3421,"USA",new GregorianCalendar(2018,0,1),new GregorianCalendar(2020,0,1));
		Passport pThree=new Passport(4312,"wakanda",new GregorianCalendar(2018,9,1),new GregorianCalendar(2022,0,1));
		EmployeeAssignThree empOne=new EmployeeAssignThree(1,"Thor",pOne);
		EmployeeAssignThree empTwo=new EmployeeAssignThree(2,"Tony Stark",pTwo);
		EmployeeAssignThree empThree=new EmployeeAssignThree(3,"TChalla",pThree);
		session.beginTransaction();
	
		session.saveOrUpdate(empTwo);
		session.saveOrUpdate(empOne);
		session.saveOrUpdate(empThree);
		session.getTransaction().commit();
//		session.beginTransaction();
//		session.remove(session.get(EmployeeAssignThree.class, 1));
//		session.getTransaction().commit();
	}

}
