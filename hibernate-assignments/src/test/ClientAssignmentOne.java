package test;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import entity.EmployeeAssignOne;

public class ClientAssignmentOne {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SessionFactory factory=new Configuration()
				.configure()
				.addAnnotatedClass(EmployeeAssignOne.class)
				.buildSessionFactory();

		Session session=factory.openSession();
		
			//Create 
	session.beginTransaction();
	session.saveOrUpdate(new EmployeeAssignOne(1,"Tony Stark","avenger"));
	session.saveOrUpdate(new EmployeeAssignOne(2,"Thor","avenger"));
	session.saveOrUpdate(new EmployeeAssignOne(3,"Fury","sheild"));
	session.saveOrUpdate(new EmployeeAssignOne(4,"Carter","captain"));
	session.getTransaction().commit();
	System.out.println("Inserted 3 students to table");
	//Reading One
	EmployeeAssignOne emp=session.get(EmployeeAssignOne.class,1);
	System.out.println(emp);
	//Reading All
	List<EmployeeAssignOne>empList=session.createQuery("from EmployeeAssignOne").getResultList();
	System.out.println(empList);
	//Update
	session.beginTransaction();
	emp=session.get(EmployeeAssignOne.class,2);
	emp.setName("Thor Odinson");
	session.getTransaction().commit();
	System.out.println("Update employee with id 2");
	System.out.println(emp);
	//Delete
	session.beginTransaction();
	emp=session.get(EmployeeAssignOne.class,2);
	session.remove(emp);
	System.out.println("Removed employee with id 2");
	
	}

}
