package test;

import java.util.HashSet;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import entity.DepartmentAssignTwo;
import entity.EmployeeAssignTwo;


public class ClientAssignmentTwo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SessionFactory factory=new Configuration()
				.configure()
				.addAnnotatedClass(EmployeeAssignTwo.class)
				.addAnnotatedClass(DepartmentAssignTwo.class)
				.buildSessionFactory();
		Session session=factory.openSession();
		DepartmentAssignTwo dept=new DepartmentAssignTwo(1,"research");
		dept.setEmployeeSet(new HashSet<EmployeeAssignTwo>());
		DepartmentAssignTwo deptSales=new DepartmentAssignTwo(2,"sales");
		deptSales.setEmployeeSet(new HashSet<EmployeeAssignTwo>());
		session.beginTransaction();
		session.save(dept);
		session.save(deptSales);
		EmployeeAssignTwo emp1=new EmployeeAssignTwo(1,"Thor",2300000f,deptSales);
		EmployeeAssignTwo emp2=new EmployeeAssignTwo(2,"Black Panther",1300000f,dept);
		EmployeeAssignTwo emp3=new EmployeeAssignTwo(3,"Iron Man",10300000f,dept);
		EmployeeAssignTwo emp4=new EmployeeAssignTwo(4,"Hulk",3300000f,dept);
		session.save(emp1);
		session.save(emp2);
		session.save(emp3);
		session.save(emp4);
		session.getTransaction().commit();
	}

}
